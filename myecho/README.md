# myecho

gitlab 演示项目

## 安装

npm install git@gitlab.com:yunnysunny/myecho.git#v0.1.0

需要确保 ~/.ssh/config 文件中有如下配置：

```
Host gitlab.com
  Hostname gitlab.com
  User git
  IdentityFile ~/.ssh/test_ssh
```

为了方便大家演练， test_ssh 已经防止到项目目录中了。
